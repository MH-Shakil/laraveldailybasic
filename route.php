<?php


Route::get('/', [\App\Http\Controllers\HomeController::class, 'index']);
Route::get('/user/{name}', [\App\Http\Controllers\UserController::class, 'show']);
Route::view('/about', 'pages.about')->name('about');
Route::redirect('/log-in', '/login');
Route::group(['middleware' => 'auth'], function () {
    Route::prefix('/app')->group(function () {
        Route::get('/dashboard', \App\Http\Controllers\DashboardController::class)->name('dashboard');
        Route::resource('tasks', \App\Http\Controllers\TaskController::class);
    });
    Route::group(['middleware' => 'is_admin', 'prefix' => 'admin'], function () {
        Route::get('/dashboard', \App\Http\Controllers\Admin\DashboardController::class);
        Route::get('/stats', \App\Http\Controllers\Admin\StatsController::class);
    });
});
require __DIR__.'/auth.php';
